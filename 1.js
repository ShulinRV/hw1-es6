class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  set name(value) {
    this.name = value;
  }
  get name() {
    return this._name;
  }

  set age(value) {
    this._age = value;
  }
  get age() {
    return this._age;
  }

  set salary(value) {
    this._name = value;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  set lang(value) {
    this._lang = value;
  }

  get lang() {
    return this._lang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const roman = new Programmer("Roman", 27, 1000, ["js", "java"]);

const alex = new Programmer("Alex", 26, 2000, ["c++", "python"]);

const vlad = new Programmer("Vlad", 25, 3000, ["php", "c#"]);

console.log(roman);
console.log(alex);
console.log(vlad);
console.log(roman.salary);
console.log(alex.salary);
console.log(vlad.salary);
